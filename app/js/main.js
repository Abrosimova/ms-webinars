import { Cookie } from './modules/cookie.js'
import { API } from './modules/api.js'
import { Auth } from './modules/auth.js'
import { Activity } from './modules/activity.js'
import { appOptions } from './modules/settings.js'
import { Loader } from './modules/loader.js'
import { Modal } from './modules/modal.js'

var App = {
  webinars: {
    1: new Date('2017-11-01T15:00:00'),
    2: new Date('2017-11-07T15:00:00'),
    3: new Date('2017-11-14T15:00:00'),
    4: new Date('2017-11-21T15:00:00'),
    5: new Date('2017-11-28T15:00:00'),
    6: new Date('2017-12-05T15:00:00'),
  },
  comments: [],
  init: function () {
    var that = this;

    API.init();
    that.webinarId = $('.comments-container').data('webinar');
    if (document.location.href.indexOf('access_token') > -1) {
      Auth.auth(function() {
        API.token = Auth.token;
        API.getUserInfo();
        var path = window.location.pathname.substring(0, window.location.pathname.length);

        history.pushState('', document.title, path);
        that.checkSession();
      });
    } else {
      this.checkSession();
    }
    
    $('.main-page').on('focus', '.web__textarea', function() {
      $('.web__warning').empty();
    });

    $('.main-page').on('click', '.send-comment', function() {
      var val = $.trim($('.web__textarea').val());

      if (val.length > 1500) {
        $('.web__warning').html('Длина комментария не должна превышать 1500 символов');

        return;
      }
      if (val.length === 0) {        
        return;
      }
      Loader.show();
      
      API.addKeyToDB({
        label: appOptions.provider + '-webinar-' + that.webinarId,
        key: appOptions.provider + '-webinar_comment-' + that.webinarId + '_' + new Date().getTime(),
        value: JSON.stringify({ 
          text: val, 
          user: { 
            id: API.model.user.id_str, 
            photo: API.model.user.photoMedium, 
            name: API.model.user.firstName + ' ' + API.model.user.lastName 
          } 
        })
      }, function() {
        $('.web__textarea').val('');
        Loader.hide();
        that.loadComments();
      }); 
    });

    $('.main-page').on('click', '.next-btn', function() {
      Loader.show();
      API.getNextPage($(this).data('next'), function(data) {
        Loader.hide();
        App.drawComments(data, 1);
      })
    });
    
    $('.main-page').on('click', '.refresh-comment', function() {
      that.loadComments();
    });

    $('.main-page').on('click', '.comment__delete', function() {
      var el = $(this).parent();

      API.deleteKeyFromDB($(this).data('id'), function() {
        el.remove();
      })
    });
    
    $('.main-page').on('click', '.ans-comment', function() {
      if ($(this).parent().children('.comment-answer').length > 0) {
        return;
      }
      var html = '';
      
      html += '<div class="comment-answer">'
      html += '<textarea class="ans-comment-textarea" placeholder="Введите текст"></textarea>'
      html += '<div class="ans-comment__warning"></div>'
      html += '<button class="ms-btn comment-answer__btn">Отправить</button>'
      html += '</div>'
      $(this).parent().append(html);
    });
    
    $('.main-page').on('click', '.comment-answer__btn', function() {
      var val = $.trim($(this).parent().children('.ans-comment-textarea').val());

      if (val.length > 1500) {
        $(this).parent().children('.ans-comment__warning').html('Длина комментария не должна превышать 1500 символов');

        return;
      }
      if (val.length === 0) {        
        return;
      }
      Loader.show();
      
      var id = $(this).parents('.comment').data('id');
      var comment = that.comments.filter(function(item) {
        return item.id === id;
      })[0];
      
      comment.comment = {
        text: val, 
        user: { 
          id: API.model.user.id_str, 
          photo: API.model.user.photoMedium, 
          name: API.model.user.firstName + ' ' + API.model.user.lastName 
        }  
      };
      API.addKeyToDB({
        label: appOptions.provider + '-webinar-' + that.webinarId,
        key: id,
        value: JSON.stringify(comment)
      }, function() {
        Loader.hide();
        that.loadComments();
      }); 
    });
    
    $('.main-page').on('click', '.comment-answer__delete', function() {
      Loader.show();
      
      var id = $(this).parents('.comment').data('id');
      var comment = that.comments.filter(function(item) {
        return item.id === id;
      })[0];
      
      delete comment.comment;
      API.addKeyToDB({
        label: appOptions.provider + '-webinar-' + that.webinarId,
        key: id,
        value: JSON.stringify(comment)
      }, function() {
        Loader.hide();
        that.loadComments();
      }); 
    });
  },
  
  checkSession() {
    var that = this;
    
    if (void 0 !== Cookie.get(appOptions.provider + '_token')) {
      API.token = Cookie.get(appOptions.provider + '_token');
      Loader.show();
      API.getUserInfo(function() {
        Loader.hide();
        if (API.model.user.roles.indexOf('EduParent') === -1 && API.model.user.roles.indexOf('EduStaff') === -1) {
          var html = '';
      
          html += '<div class="auth__text">Данный проект доступен только для родителей и учителей</div>'
          Modal.open(html, null, 1);
        } else {
          /* eslint-disable */
          API.getKeysFromDB({
            label: appOptions.provider + '-activity',
            pageSize: 100,
            pageNumber: 1
          }, function (data) {
            if (data.Paging.next) {
              API.getRecursivePage(data.Paging.next, function (data) {
                data = data.Keys;
                var count = [];

                for (var i = 0; i < data.length; i++) {
                  var json = JSON.parse(data[i].Value);
                  if (Object.keys(json.webinars).length > 3) {
                    for (var j in json.webinars) {
                      json.webinars[j] = new Date(json.webinars[j]).toDateString()
                    }
                    count.push(json)
                  }
                }
                console.log(JSON.stringify(count));
              }, data);
            }
          });
          /* eslint-enable */
          Activity.get(function() {
            var today = new Date().getTime();
            var webinarStart = that.webinars[that.webinarId].getTime();
            
            if (today > webinarStart && today < webinarStart + (5 * 24 * 60 * 60 * 1000)) {
              Activity.model.webinars[that.webinarId] = today;
              Activity.set();
            }
          });
          that.loadComments();
        }        
      });
    } else {
      var html = '';
      
      html += '<div class="auth__text">Для получения сетрификатов использование данных Вашей учетной записи обязательно.</div>'
      html += '<button class="ms-btn auth__btn">Авторизация</button>'
      html += '<button class="ms-btn no-auth__btn">Продолжить без авторизации</button>'
      Modal.open(html, null, 1);
      
      $('.auth__btn').on('click', function() {
        Auth.auth();
      });
      
      $('.no-auth__btn').on('click', function() {
        $('.web__textarea').remove();
        $('.ms-btn').remove();
        Modal.close();
      });
    }
  },
  
  loadComments: function() {
    var that = this;
    
    Loader.show();
    API.getKeysFromDB({
      label: appOptions.provider + '-webinar-' + that.webinarId,
      pageSize: 10,
      pageNumber: 1,
      orderBy: 'date_desc'
    }, function (data) {
      Loader.hide();
      that.comments = [];
      that.drawComments(data);
    })
  },
  
  getTimeStamp: function(parseDate) {
    var date = new Date(new Date(parseDate).getTime() - new Date().getTimezoneOffset() * 60000);
    
    return (date.getDate() < 10 ? ('0' + date.getDate()) : date.getDate()) + '.' + (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)) + '.' + date.getFullYear() + ', ' + (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes())
  },

  drawComments: function(data, nextPage) {
    var html = '',
      json;
    
    for (var i = 0; i < data.Keys.length; i++) {
      json = JSON.parse(data.Keys[i].Value);
      if (!json.text) {
        continue;
      }
      json.id = data.Keys[i].Key;
      this.comments.push(json);
      html += '<div class="comment" data-id="' + json.id + '">'
      html += '<img class="comment__img" src="' + json.user.photo + '">'
      html += '<div class="comment__title">' + json.user.name + ' / ' + this.getTimeStamp(data.Keys[i].CreatedDate) + '</div>'
      html += '<div class="comment__text">' + json.text + '</div>'
      html += appOptions.admins.indexOf(API.model.user.id_str) > -1 ? '<span class="comment__delete" data-id="' + data.Keys[i].Key + '">Удалить</span>' : '';
      if (json.comment === undefined && appOptions.admins.indexOf(API.model.user.id_str) > -1) {
        html += '<button class="ms-btn ans-comment">Ответить</button>'
      } else if (json.comment !== undefined) {
        html += '<div class="comment-answer comment-answer-text">'
        html += '<img class="comment__img comment-answer__img" src="' + json.comment.user.photo + '">'
        html += '<div class="comment__title">' + json.comment.user.name + '</div>'
        html += '<div class="ans-comment__text">' + json.comment.text + '</div>'
        html += appOptions.admins.indexOf(API.model.user.id_str) > -1 ? '<span class="comment-answer__delete">Удалить</span>' : '';
        html += '</div>'
      }
      html += '</div>'
    }
    if (nextPage === undefined) {
      if (data.Paging.next !== null) {
        html += '<button class="next-btn ms-btn" data-next="' + data.Paging.next + '">Загрузить ещё</button>'
      }
      $('.comments-container').empty().append(html);
    } else {
      $('.next-btn').before(html);
      $('.next-btn').data('next', data.Paging.next);
      if (data.Paging.next === null) {
        $('.next-btn').remove();
      }
    }
  }
}

$(document).ready(function() {
  App.init();
})
