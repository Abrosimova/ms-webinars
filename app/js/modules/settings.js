var isMobile = (function() {
  if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i)
  || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)
  || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i)
  || navigator.userAgent.match(/Windows Phone/i)) {
    return true;
  }

  return false;
})();

var locationUrl = document.location.href,
  appOptions;

if (locationUrl.indexOf('school.mosreg') > -1) {
  appOptions = {
    authUrl: 'https://login.school.mosreg.ru/oauth2',
    grantUrl: 'https://api.school.mosreg.ru/v1/authorizations',
    scope: '',
    clientId: '',
    redirectUrl: window.location.href + '/?auth=true',
    provider: 'mosreg-ms_webinar',
    api: 'https://api.school.mosreg.ru/v1/',
    isMobile: isMobile,
    userLink: 'https://school.mosreg.ru/user/user.aspx?user=',
    admins: ['1000005813233', '1000005762061'],
    cdnPath: 'https://ad.csdnevnik.ru/special/staging/ms-webinar/img/'
  }
} else {
  appOptions = {
    authUrl: 'https://login.dnevnik.ru/oauth2',
    grantUrl: 'https://api.dnevnik.ru/v1/authorizations',
    scope: 'CommonInfo',
    clientId: '0fe0c88bc7c2451b828aad8aa5097cec',
    redirectUrl: window.location.href + '?auth=true',
    provider: 'ms_webinar',
    api: 'https://api.dnevnik.ru/v1/',
    isMobile: isMobile,
    userLink: 'https://dnevnik.ru/user/user.aspx?user=',
    admins: ['1000006435101', '1000006315838', '1000004681017', '1000006981364', '1000006989964'],
    cdnPath: 'https://ad.csdnevnik.ru/special/staging/ms-webinar/img/'
  }
}

export { appOptions }
