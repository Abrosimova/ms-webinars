import { appOptions } from './settings.js'
import { API } from './api.js'
//  import { Content } from './content.js'

var Activity = {
  constructor: Activity,
  get: function(callback) {
    var that = this;

    API.getKeyFromDB({ key: appOptions.provider + '-activity-user_' + API.model.user.id_str }, function(data) {
//      data = null;
      if (data) {
        that.model = JSON.parse(data.Value);
        if (typeof callback === 'function') {
          callback(data === null);
        }
      } else {
        that.model = {
          webinars: {},
          user: API.model.user
        };
        that.set();
        if (typeof callback === 'function') {
          callback();
        }    
      }
    });
  },

  set: function(callback) {
    var that = this;
    
    API.addKeyToDB({
      label: appOptions.provider + '-activity',
      key: appOptions.provider + '-activity-user_' + API.model.user.id_str,
      value: JSON.stringify(that.model)
    }, function() {
      if (typeof callback === 'function') {
        callback();
      }
    });
  }
}

export { Activity }
