/* global ymaps */
import { appOptions } from './settings.js'

var Shops = {
  model: [
    {
      city: 'Воронеж',
      name: 'Hoff Воронеж',
      address: 'Рамонский р-н, п.Солнечный, ул. Парковая, 3., Сити-парк "Град"',
      type: 1,
      pos: [51.788332, 39.205264]
    },

    {
      city: 'Екатеринбург',
      name: 'Hoff Екатеринбург',
      address: 'ул. Репина, д. 94., ТРЦ "РАДУГА-ПАРК"',
      type: 1,
      pos: [56.816777, 60.538600]
    },

    {
      city: 'Краснодар',
      name: 'Hoff Красная площадь',
      address: 'ул.Дзержинского, д.100, ТРЦ "Красная Площадь"',
      type: 1,
      pos: [45.102460, 38.984276]
    },

    {
      city: 'Казань',
      name: 'Hoff Казань',
      address: 'ул.Рихарда Зорге 11 «Б», ТЦ "ГоркиПарк"',
      type: 1,
      pos: [55.764739, 49.185524]
    },

    {
      city: 'Краснодар',
      name: 'Hoff Краснодар',
      address: 'ул. Крылатая, д. 2., ТРК "OZ"',
      type: 1,
      pos: [45.011569, 39.122641]
    },

    {
      city: 'Москва',
      name: 'Hoff Автозаводская',
      address: 'ул. Ленинская Слобода, д.26, Мебельный центр ROOMER., ТЦ "ROOMER"',
      type: 1,
      pos: [55.710392, 37.654759]
    },

    {
      city: 'Москва',
      name: 'Hoff Ходинское поле',
      address: 'Ходынский бульвар, 4, ТРЦ "Авиа Парк"',
      type: 1,
      pos: [55.790607, 37.531663]
    },

    {
      city: 'Москва',
      name: 'Hoff Жулебино',
      address: 'МКАД, 8км (внешняя сторона), ТЦ "Драйв"',
      type: 1,
      pos: [55.702824, 37.834109]
    },

    {
      city: 'Москва',
      name: 'Hoff Минское шоссе',
      address: 'Московская область, Одинцовский район, пос. Новоивановское, ул. Западная, стр. 4., ТЦ "Пикник"',
      type: 1,
      pos: [55.708370, 37.359435]
    },

    {
      city: 'Москва',
      name: 'Hoff Домодедовская',
      address: 'Каширское шоссе, д. 61, корп. 3А, ТЦ "Москва"',
      type: 1,
      pos: [55.616382, 37.716260]
    },

    {
      city: 'Москва',
      name: 'Hoff Теплый Стан',
      address: 'Калужское шоссе, д.4 стр.1 (41 км. МКАД), ТЦ "Славянский Стан"',
      type: 1,
      pos: [55.609519, 37.487547]
    },

    {
      city: 'Москва',
      name: 'Hoff Home Метрополис',
      address: 'Ленинградское шоссе, д.16А, стр.4, ТЦ "Метрополис"',
      type: 2,
      pos: [55.822750, 37.496908]
    },

    {
      city: 'Москва',
      name: 'Hoff Home Кутузовский',
      address: 'Кутузовский проспект, д.57, ТЦ "Океания"',
      type: 2,
      pos: [55.727530, 37.476833]
    },

    {
      city: 'Москва',
      name: 'Hoff Home Полежаевская',
      address: 'Хорошевское шоссе д.33/1, ТРЦ «Хорошо!», 3 этаж',
      type: 2,
      pos: [55.777483, 37.523753]
    },

    {
      city: 'Москва',
      name: 'Hoff Home Афимолл',
      address: 'Пресненская наб. д.2, ТРЦ "Афимолл"',
      type: 2,
      pos: [55.749294, 37.539343]
    },

    {
      city: 'Москва',
      name: 'Hoff Home Колумбус',
      address: 'ул. Кировоградская, 13А, ТРЦ Columbus',
      type: 2,
      pos: [55.612182, 37.607348]
    },

    {
      city: 'Москва',
      name: 'Hoff на Новой Риге',
      address: 'Московская область, Красногорский р-н, вблизи д. Воронки, 23-й км а/м "Балтия", корпус 1, ТЦ "Юнимолл"',
      type: 1,
      pos: [55.800113, 37.280357]
    },

    {
      city: 'Москва',
      name: 'Hoff Белая дача',
      address: 'Московская обл., Люберецкий р-н, г. Котельники, 1-й Покровский пр-д, д. 5, ТЦ "Мега"',
      type: 1,
      pos: [55.653270, 37.844402]
    },

    {
      city: 'Химки',
      name: 'Hoff Химки',
      address: 'МО, г. Химки, Ленинградское ш., 1/1, ТЦ "HOFF"',
      type: 1,
      pos: [55.884116, 37.440084]
    },

    {
      city: 'Мытищи',
      name: 'Hoff Мытищи',
      address: 'г. Мытищи, улица Мира, дом 51, ТРЦ "Июнь"',
      type: 1,
      pos: [55.919768, 37.708561]
    },

    {
      city: 'Новороссийск',
      name: 'Hoff Mini Новороссийск',
      address: 'ул. Героев Десантников, 2, ТРК "Южный пассаж"',
      type: 3,
      pos: [44.685264, 37.781281]
    },

    {
      city: 'Подольск',
      name: 'Hoff Mini Подольск',
      address: 'проспект Юных Ленинцев д.61, ГМ «Шмель»',
      type: 3,
      pos: [55.481882, 37.564456]
    },

    {
      city: 'Ростов-на-Дону',
      name: 'Hoff Mini Ростов-на-Дону',
      address: 'ул.Малиновского, 27А',
      type: 3,
      pos: [47.231366, 39.612739]
    },

    {
      city: 'Ростов-на-Дону',
      name: 'Hoff Ростов-на-Дону',
      address: 'пр-т Михаила Нагибина, д. 32, к. 2.',
      type: 1,
      pos: [47.257714, 39.720336]
    },

    {
      city: 'Самара',
      name: 'Hoff Самара',
      address: 'Московское ш., д. 106.',
      type: 1,
      pos: [53.223651, 50.189753]
    },

    {
      city: 'Санкт-Петербург',
      name: 'Hoff Санкт-Петербург',
      address: 'Пулковское шоссе д.19 А, ТЦ "HOFF"',
      type: 1,
      pos: [59.825725, 30.316870]
    },

    {
      city: 'Сочи',
      name: 'Hoff Mini Сочи',
      address: 'ул. Новая Заря, 7 , ТРЦ "Моремолл"',
      type: 3,
      pos: [43.605861, 39.732190]
    },
  ],

  getIcons: function() {
    for (var i = 0; i < this.model.length; i++) {
      switch (this.model[i].type) {
        case 1:
          this.model[i].icon = appOptions.cdnPath + 'hoff-icon.png'
          break;
        case 2:
          this.model[i].icon = appOptions.cdnPath + 'home-icon.png'
          break;
        case 3:
          this.model[i].icon = appOptions.cdnPath + 'mini-icon.png'
          break;

      }
    }
  },

  getList: function(pos) {
    this.getIcons();

    return this.model.sort(function(a, b) {
      if (ymaps.coordSystem.geo.getDistance(a.pos, pos) > ymaps.coordSystem.geo.getDistance(b.pos, pos)) {
        return 1
      } else if (ymaps.coordSystem.geo.getDistance(a.pos, pos) < ymaps.coordSystem.geo.getDistance(b.pos, pos)) {
        return -1
      }

      return 0
    })

  },

  getClosest: function(pos) {
    var res = [];

    for (var i = 0; i < this.model.length; i++) {
      if (ymaps.coordSystem.geo.getDistance(this.model[i].pos, pos) < 100000) {
        res.push(this.model[i])
      }
    }

    return res;
  }
}

// 1 - Гипермаркет
// 2 - Home
// 3 - Mini

export { Shops }
